let container = document.querySelector('.container')
let form = document.querySelector('form')
let inputText = document.getElementById('text')
let addButton = document.getElementById('addButton')
let cardMainContainer = document.querySelector('.card-main-container')
let popUpCloseBtn = document.querySelector('#closeBtn')
let popUpContainer = document.querySelector('.popUpContainer')
let saveDescription = document.querySelector('#saveDescription')
let closeDescription = document.querySelector('#closeDescription')
let editDescription = document.querySelector('#edit')
let popUpShows = document.querySelector('.popUpShows')
let tempObj = {}
let cardId = {}



let key = 'ca306ee3bb37dcc0aefd448047b05d63'
let token = 'ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d'
let idList = '623c514d479b352c1afa6f66'
let boardId = '623b01029e18977250a4d91a'





addButton.addEventListener('click', (event) => {
    if (inputText.value.trim()) {

        event.preventDefault()

        // CREATING UL TAG
        let ulTag = document.createElement('ul')
        ulTag.classList.add('cards-container')



        // CREATING CARD DIV
        let cards = document.createElement('div')
        cards.classList.add('card')


        // CREATING ITEM LI
        let liTag = document.createElement('li')
        liTag.classList.add('item')
        liTag.innerHTML = inputText.value

        // CREATING DELETE BUTTON
        let button = document.createElement('button')
        button.classList.add('submit-btn')
        button.innerHTML = 'DELETE'

        // CREATING INNER INPUT
        let input = document.createElement('input')
        input.setAttribute('type', 'text')

        //CREATING ITEM DIV
        let allItems = document.createElement('div')
        allItems.classList.add('allItems')


        // CREATING ADD BUTTON
        let inputbtn = document.createElement('button')
        inputbtn.classList.add('add-btn')
        inputbtn.innerHTML = 'ADD'

        // APPENDING UL TAG TO THE MAIN CONTAINER
        cardMainContainer.appendChild(ulTag)

        // APPENDING CHILD TO THE UL TAG
        ulTag.appendChild(cards)
        cards.appendChild(liTag)
        cards.appendChild(allItems)
        cards.appendChild(input)
        cards.appendChild(inputbtn)
        cards.appendChild(button)

        container.appendChild(cardMainContainer)

        // CREATING CARDS
        let items = document.createElement('li')
        items.innerHTML = input.value //SET THE INPUT VALUE
        items.classList.add('task')
        allItems.appendChild(items)

        // ADDING CARDS TO TRELLO
        fetch(`https://api.trello.com/1/lists?name=${liTag.innerText}&idBoard=${boardId}&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let obj = JSON.parse(text)
                let objId = obj['id']
                tempObj[liTag.innerText] = objId


            })
            .catch(err => console.error(err));




        // DELETE BUTTON CLICK FUNCTION
        button.addEventListener('click', (event) => {
            event.target.parentElement.parentElement.remove()
            let listName = event.target.parentElement.firstChild.innerText

            fetch(`https://api.trello.com/1/lists/${tempObj[listName]}/closed?value=true&key=${key}&token=${token}`, {
                method: 'PUT'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })
                .then(text => console.log(text))
                .catch(err => console.error(err));

        })


        // INSIDE INPUT TEST
        inputbtn.addEventListener('click', add_card)
        function add_card() {
            // CREATING TASK LI
            let items = document.createElement('li')
            items.innerHTML = input.value //SET THE INPUT VALUE
            let value = liTag.textContent

            fetch(`https://api.trello.com/1/cards?name=${input.value}&idList=${tempObj[value]}&key=${key}&token=${token}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                }
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })
                .then(text => {
                    // FOR ADDING DESCRIPTION
                    let cardObj = JSON.parse(text)
                    cardId[items.innerText] = cardObj.id

                })
                .catch(err => console.error(err));


            items.classList.add('task')
            items.setAttribute('values', '0')
            allItems.appendChild(items)

            // DRAG AND DROP
            items.setAttribute('draggable', 'true')
            input.value = ''

            // DRAG AND DROP
            let allItemsContainers = document.querySelectorAll('.allItems')
            let dragItems = document.querySelectorAll('.task')
            dragItems.forEach(dragItem => {
                dragItem.addEventListener('dragstart', () => {
                    dragItem.classList.add('dragging')
                    console.log('dragstart')

                })
                dragItem.addEventListener('dragend', () => {
                    dragItem.classList.remove('dragging')
                    console.log('dragend')
                })
            })
            allItemsContainers.forEach(allItemsContainer => {
                allItemsContainer.addEventListener('dragover', () => {
                    let draggable = document.querySelector('.dragging')
                    allItemsContainer.appendChild(draggable)

                })
            })

        }

        // POPUP SHOW
        allItems.addEventListener('click', (event) => {
            if (event.target.getAttribute('values') == 0) {

                event.target.setAttribute('values', '1')

                // OVERLAY DIVE
                let overlay = document.createElement('div')
                overlay.classList.add('overlay')

                // POPUP CONTAINER
                let popUpContainer = document.createElement('div')
                popUpContainer.classList.add('popUpContainer')
                popUpContainer.classList.add(event.target.innerText)

                let header = document.createElement('header')

                let titel = document.createElement('h3')
                titel.id = 'title'

                let closeBtn = document.createElement('div')
                closeBtn.id = 'closeBtn'
                closeBtn.innerHTML = '&times;'

                let popUp = document.createElement('div')
                popUp.classList.add('popUp')

                let description = document.createElement('h3')
                description.classList.add('h3')
                description.innerText = 'Description'


                let paragraph = document.createElement('p')
                paragraph.classList.add('descriptionText')
                paragraph.innerText = ''

                let inputArea = document.createElement('textarea')
                inputArea.setAttribute('type', 'text')
                inputArea.setAttribute('placeholder', 'Add a more detailed description...')
                inputArea.classList.add('textarea')

                let descriptionBtn = document.createElement('div')
                descriptionBtn.classList.add('descriptionBtn')

                let saveDescription = document.createElement('button')
                saveDescription.id = 'saveDescription'
                saveDescription.innerText = 'SAVE'

                let closeDescription = document.createElement('button')
                closeDescription.id = 'closeDescription'
                closeDescription.innerText = 'X'

                let edit = document.createElement('button')
                edit.id = 'edit'
                edit.innerText = 'EDIT'

                // COMMENT AREA
                let commentDiv = document.createElement('div')
                commentDiv.classList.add('comments')

                let commentHeading = document.createElement('h3')
                commentHeading.innerHTML = 'Activity'
                commentDiv.appendChild(commentHeading)

                let commentInput = document.createElement('input')
                commentInput.classList.add('commentInput')
                commentInput.setAttribute('type', 'text')
                commentInput.setAttribute('placeholder', 'write comment...')
                commentDiv.appendChild(commentInput)

                let commentSave = document.createElement('button')
                commentSave.classList.add('commentSave')
                commentSave.innerHTML = 'SAVE'
                commentDiv.appendChild(commentSave)
                commentSave.style.display = 'none'

                commentInput.addEventListener('click', () => {
                    commentSave.style.display = 'block'
                })

                // COMMENT SAVE
                commentSave.addEventListener('click', () => {
                    // COMMENT INPUT BOX
                    let commentDetails = document.createElement('input')
                    commentDetails.classList.add('commentDetails')
                    commentDetails.setAttribute('type', 'text')
                    commentDiv.appendChild(commentDetails)

                    commentDetails.value = commentInput.value

                    let text = commentInput.value
                    let cardName = titel.innerText

                    // ADDING COMMENTS
                    fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/actions/comments?text=${text}&key=${key}&token=${token}`, {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json'
                        }
                    })
                        .then(response => {
                            console.log(
                                `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                        })
                        .then(text => console.log(text))
                        .catch(err => console.error(err));

                    // DELETE BUTTON
                    let deleteComment = document.createElement('a')
                    deleteComment.innerText = 'Delete'
                    deleteComment.setAttribute('href', '#')
                    commentDiv.appendChild(deleteComment)

                    deleteComment.style.display = 'block'
                    commentInput.value = ''
                    commentSave.style.display = 'none'

                    // DELETE COMMENT
                    deleteComment.addEventListener('click', () => {
                        commentDetails.style.display = 'none'
                        deleteComment.style.display = 'none'
                    })
                })

                // COMMENT END


                descriptionBtn.append(saveDescription, closeDescription, edit)

                popUpContainer.append(header, popUp, descriptionBtn, commentDiv)
                header.append(titel, closeBtn)
                popUp.append(description, paragraph, inputArea)

                popUpShows.append(overlay, popUpContainer)

                popUpContainer.style.display = 'block'
                overlay.style.display = 'block'
                saveDescription.style.display = 'block'

                titel.innerHTML = event.target.textContent

                // POP CLOSE
                closeBtn.addEventListener('click', () => {
                    popUpContainer.style.display = 'none'
                    overlay.style.display = 'none'


                })

                // SAVE BUTTON FUNCTION
                saveDescription.addEventListener('click', (e) => {
                    paragraph.innerHTML = inputArea.value
                    edit.style.display = 'block'
                    saveDescription.style.display = 'none'
                    closeDescription.style.display = 'block'
                    inputArea.style.display = 'none'

                    let desc = inputArea.value
                    let cardName = titel.innerText
                    fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                        method: 'PUT',
                        headers: {
                            'Accept': 'application/json'
                        }
                    })
                        .then(response => {
                            console.log(
                                `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                        })
                        .then(text => console.log(text))
                        .catch(err => console.error(err));

                    inputArea.value = ''
                })

                // CLOSE BUTTON FUNCTION
                closeDescription.addEventListener('click', () => {
                    inputArea.style.display = 'none'

                })

                // EDIT BUTTON FUNCTION
                edit.addEventListener('click', (e) => {
                    if (e.target.textContent == "EDIT") {
                        inputArea.style.display = 'block'
                        inputArea.value = paragraph.innerText
                        paragraph.innerHTML = inputArea.value
                        e.target.innerHTML = 'SAVE'

                        let desc = inputArea.value
                        let cardName = titel.innerText
                        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                            method: 'PUT',
                            headers: {
                                'Accept': 'application/json'
                            }
                        })
                            .then(response => {
                                console.log(
                                    `Response: ${response.status} ${response.statusText}`
                                );
                                return response.text();
                            })
                            .then(text => console.log(text))
                            .catch(err => console.error(err));

                    } else {
                        paragraph.innerHTML = inputArea.value
                        e.target.innerHTML = 'EDIT'
                        let desc = inputArea.value
                        let cardName = titel.innerText
                        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                            method: 'PUT',
                            headers: {
                                'Accept': 'application/json'
                            }
                        })
                            .then(response => {
                                console.log(
                                    `Response: ${response.status} ${response.statusText}`
                                );
                                return response.text();
                            })
                            .then(text => console.log(text))
                            .catch(err => console.error(err));
                    }
                })
            }
            console.log(document.querySelector(`.${event.target.innerText}`))

            if (event.target.getAttribute('values') == 1) {
                document.querySelector(`.${event.target.innerText}`).style.display = 'block'
            }
        })

        form.reset()
    }
    inputText.value = ''


})


let renderCards = (data) => {
    getCard(data)

    data.forEach(cards => {
        output += `
        <ul class="cards-container">
                <div class="card" data-id=${cards.id}>
                    <li class="item">${cards.name}</li>
                    <div class="allItems">
                    
                    </div>
                    <input type="text">
                    <button class="add-btn">ADD</button>
                    <button class="submit-btn">DELETE</button>
                </div>
            </ul>
        `
    })
    cardMainContainer.innerHTML = output
}

// GETTING LISTS
let obj = {}
let output = ''
fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
    method: 'GET',
    headers: {
        'Accept': 'application/json'
    }
})
    .then(res => res.json())

    .then(data => {
        data.forEach((element) => {
            tempObj[element.name] = element.id
        })
        renderCards(data)
    })



// DELETE LIST
cardMainContainer.addEventListener('click', (e) => {
    let addCard = e.target.className == 'add-btn'
    let listName = e.target.parentElement.firstElementChild.innerText
    if (addCard) {
        let allItems = e.target.previousElementSibling.previousElementSibling
        let li = document.createElement('li')
        li.classList.add('task')
        allItems.appendChild(li)
        li.innerText = e.target.previousElementSibling.value


        // POSTING CARDS
        fetch(`https://api.trello.com/1/cards?name=${li.innerText}&idList=${tempObj[listName]}&key=${key}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => {
                let cardObj = JSON.parse(text)
                render[li.innerText] = cardObj.id
            })
            .catch(err => console.error(err));
    }


    // DELETING CARDS
    let id = e.target.parentElement.dataset.id
    let deleteBtnPressed = e.target.className == 'submit-btn'
    if (deleteBtnPressed) {
        fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`, {
            method: 'PUT'
        })
            .then(res => res.json())
            .then(() => location.reload())
    }
})

// GETTING CARDS
function getCard(text) {
    text.forEach(card => {
        let obj = {}
        let id = card.id
        let cardName = card.name
        obj[cardName] = id

        fetch(`https://api.trello.com/1/lists/${id}/cards?key=ca306ee3bb37dcc0aefd448047b05d63&token=ab25220c61d4bfadce681521aa57499eea73f31867f52b7227737eadbd7af39d`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(res => res.json())
            .then(text => text.forEach(value => {
                let allItems = document.createElement('div')
                allItems.classList.add('allItems')
                let lists = document.querySelectorAll('.allItems')
                lists.forEach((list) => {
                    let name = list.previousElementSibling.innerHTML
                    if (obj[name] == value.idList) {
                        let li = document.createElement('li')
                        li.classList.add('task')
                        li.setAttribute('values', '0')
                        li.setAttribute('draggable', 'true')
                        li.innerText = value.name
                        list.appendChild(li)

                        // POP UP
                        li.addEventListener('click', () => {
                            if (li.getAttribute('values') == 0) {
                                li.setAttribute('values', '1')

                                let overlay = document.createElement('div')
                                overlay.classList.add('overlay')

                                let popUpContainer = document.createElement('div')
                                popUpContainer.classList.add('popUpContainer')
                                popUpContainer.classList.add(li.innerText)
                                popUpContainer.style.display = 'block'

                                let header = document.createElement('header')

                                let titel = document.createElement('h3')
                                titel.id = 'title'
                                titel.innerText = 'title'

                                let closeBtn = document.createElement('div')
                                closeBtn.id = 'closeBtn'
                                closeBtn.innerHTML = '&times;'

                                let popUp = document.createElement('div')
                                popUp.classList.add('popUp')

                                let description = document.createElement('h3')
                                description.classList.add('h3')
                                description.innerText = 'Description'


                                let paragraph = document.createElement('p')
                                paragraph.classList.add('descriptionText')
                                paragraph.innerText = ''

                                let inputArea = document.createElement('textarea')
                                inputArea.setAttribute('type', 'text')
                                inputArea.setAttribute('placeholder', 'Add a more detailed description...')
                                inputArea.classList.add('textarea')


                                let descriptionBtn = document.createElement('div')
                                descriptionBtn.classList.add('descriptionBtn')

                                let saveDescription = document.createElement('button')
                                saveDescription.id = 'saveDescription'
                                saveDescription.innerText = 'SAVE'

                                let closeDescription = document.createElement('button')
                                closeDescription.id = 'closeDescription'
                                closeDescription.innerText = 'X'

                                let edit = document.createElement('button')
                                edit.id = 'edit'
                                edit.innerText = 'EDIT'


                                let commentDiv = document.createElement('div')
                                commentDiv.classList.add('comments')

                                let commentHeading = document.createElement('h3')
                                commentHeading.innerHTML = 'Activity'
                                commentDiv.appendChild(commentHeading)

                                let commentInput = document.createElement('input')
                                commentInput.classList.add('commentInput')
                                commentInput.setAttribute('type', 'text')
                                commentInput.setAttribute('placeholder', 'write comment...')
                                commentDiv.appendChild(commentInput)

                                let commentSave = document.createElement('button')
                                commentSave.classList.add('commentSave')
                                commentSave.innerHTML = 'SAVE'
                                commentDiv.appendChild(commentSave)
                                commentSave.style.display = 'none'

                                commentInput.addEventListener('click', () => {
                                    commentSave.style.display = 'block'
                                })

                                // COMMENT SAVE
                                commentSave.addEventListener('click', () => {
                                    // COMMENT INPUT BOX
                                    let commentDetails = document.createElement('input')
                                    commentDetails.classList.add('commentDetails')
                                    commentDetails.setAttribute('type', 'text')
                                    commentDiv.appendChild(commentDetails)

                                    commentDetails.value = commentInput.value

                                    let text = commentInput.value
                                    let cardName = titel.innerText

                                    // ADDING COMMENTS
                                    fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/actions/comments?text=${text}&key=${key}&token=${token}`, {
                                        method: 'POST',
                                        headers: {
                                            'Accept': 'application/json'
                                        }
                                    })
                                        .then(response => {
                                            console.log(
                                                `Response: ${response.status} ${response.statusText}`
                                            );
                                            return response.text();
                                        })
                                        .then(text => console.log(text))
                                        .catch(err => console.error(err));

                                    // DELETE BUTTON
                                    let deleteComment = document.createElement('a')
                                    deleteComment.innerText = 'Delete'
                                    deleteComment.setAttribute('href', '#')
                                    commentDiv.appendChild(deleteComment)

                                    deleteComment.style.display = 'block'
                                    commentInput.value = ''
                                    commentSave.style.display = 'none'

                                    // DELETE COMMENT
                                    deleteComment.addEventListener('click', () => {
                                        commentDetails.style.display = 'none'
                                        deleteComment.style.display = 'none'
                                    })
                                })

                                // COMMENT END

                                descriptionBtn.append(saveDescription, closeDescription, edit)

                                popUpContainer.append(header, popUp, descriptionBtn, commentDiv)
                                header.append(titel, closeBtn)
                                popUp.append(description, paragraph, inputArea)

                                popUpShows.append(overlay, popUpContainer)

                                popUpContainer.style.display = 'block'
                                overlay.style.display = 'block'
                                saveDescription.style.display = 'block'

                                titel.innerHTML = li.textContent

                                // POP CLOSE
                                closeBtn.addEventListener('click', () => {
                                    popUpContainer.style.display = 'none'
                                    overlay.style.display = 'none'


                                })


                                saveDescription.addEventListener('click', (e) => {
                                    paragraph.innerHTML = inputArea.value
                                    edit.style.display = 'block'
                                    saveDescription.style.display = 'none'
                                    closeDescription.style.display = 'block'
                                    inputArea.style.display = 'none'

                                    let desc = inputArea.value
                                    let cardName = titel.innerText


                                    fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                                        method: 'PUT',
                                        headers: {
                                            'Accept': 'application/json'
                                        }
                                    })
                                        .then(response => {
                                            console.log(
                                                `Response: ${response.status} ${response.statusText}`
                                            );
                                            return response.text();
                                        })
                                        .then(text => console.log(text))
                                        .catch(err => console.error(err));

                                    inputArea.value = ''
                                })


                                // CLOSE BUTTON FUNCTION
                                closeDescription.addEventListener('click', () => {
                                    inputArea.style.display = 'none'

                                })

                                // EDIT BUTTON FUNCTION
                                edit.addEventListener('click', (e) => {
                                    if (e.target.textContent == "EDIT") {
                                        inputArea.style.display = 'block'
                                        inputArea.value = paragraph.innerText
                                        paragraph.innerHTML = inputArea.value
                                        e.target.innerHTML = 'SAVE'

                                        let desc = inputArea.value
                                        let cardName = titel.innerText
                                        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                                            method: 'PUT',
                                            headers: {
                                                'Accept': 'application/json'
                                            }
                                        })
                                            .then(response => {
                                                console.log(
                                                    `Response: ${response.status} ${response.statusText}`
                                                );
                                                return response.text();
                                            })
                                            .then(text => console.log(text))
                                            .catch(err => console.error(err));

                                    } else {
                                        paragraph.innerHTML = inputArea.value
                                        e.target.innerHTML = 'EDIT'
                                        let desc = inputArea.value
                                        let cardName = titel.innerText

                                        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                                            method: 'PUT',
                                            headers: {
                                                'Accept': 'application/json'
                                            }
                                        })
                                            .then(response => {
                                                console.log(
                                                    `Response: ${response.status} ${response.statusText}`
                                                );
                                                return response.text();
                                            })
                                            .then(text => console.log(text))
                                            .catch(err => console.error(err));
                                    }
                                })
                            }
                            if (li.getAttribute('values') == 1) {
                                document.querySelector(`.${li.innerText}`).style.display = 'block'
                            }
                        })


                        // DRAG DROP
                        let allItemsContainers = document.querySelectorAll('.allItems')
                        let dragItems = document.querySelectorAll('.task')
                        dragItems.forEach(dragItem => {
                            dragItem.addEventListener('dragstart', () => {
                                dragItem.classList.add('dragging')
                                console.log('dragstart')

                            })
                            dragItem.addEventListener('dragend', () => {
                                dragItem.classList.remove('dragging')
                                console.log('dragend')
                            })
                        })
                        allItemsContainers.forEach(allItemsContainer => {
                            allItemsContainer.addEventListener('dragover', () => {
                                let draggable = document.querySelector('.dragging')
                                allItemsContainer.appendChild(draggable)

                            })
                        })
                    }
                })
            }))
            .catch(err => console.error(err));
    })
}
